#include <stdio.h>
#include <packedobjects/packedobjects.h>
#include <errno.h>     /*  This will allown us to capture error, also the variable 'errno' used in main is defined in this header file*/

#define XML_SCHEMA "student.xsd"
#define MAXCHARS 256  /* Max length of array that we will define */
void add_employee_data(xmlDocPtr doc)
{
  xmlNodePtr root_node = NULL, node1 = NULL, node2 = NULL, node3 = NULL;

  root_node = xmlNewNode(NULL, BAD_CAST "company");
  xmlDocSetRootElement(doc, root_node);
  node1 = xmlNewChild(root_node, NULL, BAD_CAST "department", NULL);
  node2 = xmlNewChild(node1, NULL, BAD_CAST "employees", NULL);
  node3 = xmlNewChild(node2, NULL, BAD_CAST "employee", NULL);


  xmlNewChild(node3, NULL, BAD_CAST "id", BAD_CAST "9999");
  xmlNewChild(node3, NULL, BAD_CAST "active", BAD_CAST "true");
  xmlNewChild(node3, NULL, BAD_CAST "name", BAD_CAST "John The Doe");

  node3 = xmlNewChild(node2, NULL, BAD_CAST "employee", NULL);

  xmlNewChild(node3, NULL, BAD_CAST "id", BAD_CAST "42");
  xmlNewChild(node3, NULL, BAD_CAST "active", BAD_CAST "true");
  xmlNewChild(node3, NULL, BAD_CAST "name", BAD_CAST "Foo Bar");  
}

int main(int argc, char **argv)
{
  xmlDocPtr doc = NULL;
  packedobjectsContext *pc = NULL;
  char *pdu = NULL;
  
  char      buffer[40];                    
  unsigned id = 0;
  char *end = NULL;  
  errno =0;       /*n defined in header file 'errno.h'*/




  /* Library ID Input*/
  printf("Enter Employee ID:  ");

  if (fgets(buffer, MAXCHARS, stdin) == NULL)/*fgets is used to get input from user, MAXCHAR is constant for length and stdin means we are getting input from keyboard*/
    {
      fprintf(stderr, "failed to read string\n");
      return (EXIT_FAILURE);
    } 

  /* remove the new line character*/
  strtok(buffer, "\n");
  /* convert the string to integer*/
 id = strtoul(buffer, &end, 10);
  if(errno != 0)
    {
      fprintf(stderr, "conversion error, %d\n", strerror(errno));
      exit (EXIT_FAILURE);
    }
  else if (*end)
    {
      printf("Warning: converted partially: %i, non-convertible part: %s\n", id, end);
    }

  /* set the value of library id and activate it*/
  // lib1 = make_library(id);
  //  make_library_active(lib1);
   
  /*Library Name Input*/
   
  printf("Enter Employee  Name:  ");
  if (fgets(buffer, MAXCHARS, stdin) == NULL)
    {
      fprintf(stderr, "failed to read string\n");
      return (EXIT_FAILURE);
    }
   
  /*remove the null character*/
  strtok(buffer, "\n");
  /* set value of name in memory*/
  // set_library_name(lib1, buffer);

 












  // initialise packedobjects
  if ((pc = init_packedobjects(XML_SCHEMA, 0, 0)) == NULL) {
    printf("failed to initialise libpackedobjects");
    exit(1);
  }
  
  // create the data
  doc = xmlNewDoc(BAD_CAST "1.0");
  add_employee_data(doc);
  
  ////////////////////// Encoding //////////////////////
  
  
  // encode the XML DOM
  pdu = packedobjects_encode(pc, doc);
  if (pc->bytes == -1) {
    printf("Failed to encode with error %d.\n", pc->encode_error);
  }
  
  // free the DOM
  xmlFreeDoc(doc);
  
  ////////////////////// Decoding //////////////////////
  
  // decode the PDU into DOM
  doc = packedobjects_decode(pc, pdu);
  if (pc->decode_error) {
    printf("Failed to decode with error %d.\n", pc->decode_error);
    exit(1);
  }
  
  // output the DOM for checking
  packedobjects_dump_doc(doc);
  
  xmlFreeDoc(doc);
  
  // free memory created by packedobjects
  free_packedobjects(pc);
  
  xmlCleanupParser();
  return(0);
}
