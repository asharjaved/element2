#include <stdio.h>
#include <packedobjects/packedobjects.h>
#include <errno.h>     /*  This will allown us to capture error, also the variable 'errno' used in main is defined in this header file*/
#include "employee.h"/*header file that contains the structure of functions*/
#include <string.h>
#include <stdlib.h>


/*#include "config.h" /* auto generated*/
#define XML_SCHEMA "employee.xsd"
/*define the max length of string */
#define MAXCHARS  256
#define MAX_NAME  256

/*get the user input for eployee ID*/
char* getId(){
  char *str = NULL;
  char buffer[MAXCHARS];
  printf("Enter Employee ID:  ");
   
  if (fgets(buffer, MAXCHARS, stdin) == NULL) {   
    fprintf(stderr, "failed to read string\n");
    return (EXIT_FAILURE); //this function should return a char* always!
     
  }
  /* remove the end of line character from the string entered*/
  strtok(buffer, "\n");
  /*Strdup() function will creat a copy of array */
  str = strdup(buffer);
  return str;
}

/*get the user input for eployee Name*/
char* getName(){
  char *str = NULL;
  char buffer[MAXCHARS];
  printf("Enter Employee Name:  ");
  /* If the user does not enter anything*/
  if (fgets(buffer, MAXCHARS, stdin) == NULL) {
    fprintf(stderr,  "failed to read string\n");
    return (EXIT_FAILURE);
  }
  strtok(buffer, "\n");
  /*Strdup() function will creat a copy of array*/ 
  
 str = strdup(buffer);
  return str;
}

/*get the user input for eployee Grade*/

char* getGrade(){
  char *str = NULL;
  char buffer[MAXCHARS];
  printf("Enter Employee Grade:  ");
  /* If the user does not enter anything*/  
  if (fgets(buffer, MAXCHARS, stdin) == NULL) {
    fprintf(stderr, "failed to read string\n");
    return (EXIT_FAILURE); //this function should return a char* always!
  }
  strtok(buffer, "\n");
  /*Strdup() function will creat a copy of array*/ 
  str = strdup(buffer);
  return str;
}

/*get the user input for eployee Location*/

char* getLocation(){
  char *str = NULL;
  char buffer[MAXCHARS];
  printf("Enter Employee Location:  ");
  /* If the user does not enter anything*/
  if (fgets(buffer, MAXCHARS, stdin) == NULL) {
    fprintf(stderr, "failed to read string\n");
    return (EXIT_FAILURE); /*this function should return a char* always!*/
  }
  strtok(buffer, "\n");
  /*Strdup() function will creat a copy of array */
   str = strdup(buffer);
  return str;
}

void add_employee_data(xmlDocPtr doc)
{
  xmlNodePtr root_node = NULL, node1 = NULL,node2 = NULL,node3 = NULL,nodeName = NULL, nodeId = NULL, nodeAddress = NULL;
  char *empId = NULL;
  char *empName = NULL; 
  char *empGrade = NULL; 
  char *empLocation = NULL; 
  /*define a separate array for each of the input data entered by the user */ 
  char buffer [MAXCHARS];
  char buffer1 [MAXCHARS];
  char buffer2 [MAXCHARS];
  char buffer3 [MAXCHARS];
  char buffer4 [MAXCHARS];
  
  char *end = NULL;
  errno = 0;
  int empcount = 0;
  int num;
  /* define nodes */
  root_node = xmlNewNode(NULL, BAD_CAST "company");
  xmlDocSetRootElement(doc, root_node);
  
  node1 = xmlNewChild(root_node, NULL, BAD_CAST "department", NULL);
  node2 = xmlNewChild(node1, NULL, BAD_CAST "employees", NULL);

  /* take input how many times you want to run the programme. */
  printf("Enter Number of Employees:    ");
  if (fgets (buffer, MAXCHARS, stdin) == NULL){

    fprintf(stderr, "Enter valid input for number of employees!");
    return (EXIT_FAILURE);
  }
  strtok(buffer,"\n"); /*remove new line character*/
  /*convert the string to integer*/ 
  empcount = strtoul(buffer, &end, 10); 
  if (errno != 0){
    
    fprintf(stderr, "conversion failed %s\n", strerror(errno));
    exit (EXIT_FAILURE);

  }else if (*end){

    printf("Warning: converted partially: %i, non-convertible part: %s\n", empcount, end);

  }
  /* Take input from user for each of XML element regarding employee detail. */
  for (num =1; num <= empcount; num++){
    printf ("Details of Employee %d  \n", num);
    int varId = 0;
  again:
    while (varId ==0){
    empId = getId();
    strcpy(buffer1,empId);
    int i;
    for (i = 0; i < strlen(buffer1); i++){
      /* CHECK whether the data entered is matching the data type set in XML schema */
    if (isdigit(buffer1[i])){
      varId = 1;
    }else {
      varId = 0;
      printf ("id is not numeric   ");
      /* try again to enter right input */
      goto again;
    }
     }
   }

    int varName = 0;
  again1:
    while (varName ==0){
    empName = getName();
    strcpy(buffer2,empName);
    int j;
    for (j = 0; j < strlen(buffer2); j++){
 /* CHECK whether the data entered is matching the data type set in XML schema */
    if (!isdigit(buffer2[j])){
       varName = 1;
    }else {
      varName = 0;
      printf ("name is numeric   ");
      /* try again to enter right input */

      goto again1;
    }
     }
    }

    int varGrade = 0;
  again2:
    while (varGrade ==0){
    empGrade = getGrade();
    strcpy(buffer3,empGrade);
    int k;
    for (k = 0; k < strlen(buffer3); k++){
 /* CHECK whether the data entered is matching the data type set in XML schema */
    if (isdigit(buffer3[k])){
       varGrade = 1;
    }else {
      varGrade = 0;
      printf ("Grade is not numeric   \n");
      /* try again to enter right input */

      goto again2;
    }
     }
    }

    int varLoc = 0;
  again3:
    while (varLoc ==0){
    empLocation = getLocation();
    strcpy(buffer4,empLocation);

    int loc;
    for (loc = 0; loc < strlen(buffer4); loc++){
 /* CHECK whether the data entered is matching the data type set in XML schema */
    if (!isdigit(buffer4[loc])){
      varLoc = 1;
    }else {
      varLoc = 0;
      printf ("Location is numeric   ");
      /* try again to enter right input */

      goto again3;
    }
     }
    }

    /* enter the data entered by the user into the tree structure of XML file */    	   
    node3 = xmlNewChild(node2, NULL, BAD_CAST "employee", NULL);
  
    xmlNewChild(node3, NULL, BAD_CAST "id", BAD_CAST buffer1);
    xmlNewChild(node3, NULL, BAD_CAST "active", BAD_CAST "true");
    xmlNewChild(node3, NULL, BAD_CAST "name", BAD_CAST buffer2);
    xmlNewChild(node3, NULL, BAD_CAST "grade", BAD_CAST buffer3);
    xmlNewChild(node3, NULL, BAD_CAST "location", BAD_CAST buffer4);
  
 }

  return (EXIT_SUCCESS);  
}




