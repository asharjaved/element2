#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <packedobjects/packedobjects.h>

#define XML_SCHEMA "student.xsd"
#define MAXCHARS 256
#define NUM_STUDENTS 5

#define HOST_IP "127.0.0.1"
#define HOST_PORT 6969

int add_student_data(xmlDocPtr doc)
{
  xmlNodePtr root_node = NULL, node = NULL;
  char buffer[MAXCHARS];
  int id = 0;
  
  root_node = xmlNewNode(NULL, BAD_CAST "students");
  xmlDocSetRootElement(doc, root_node);
  for (id = 1; id <= NUM_STUDENTS; id++) {
    // lets clear our buffer each time
    bzero(buffer, MAXCHARS);
    printf("Creating student number %d\n", id);
    node = xmlNewChild(root_node, NULL, BAD_CAST "student", NULL);
    // convert integer to string
    sprintf(buffer, "%d", id);
    xmlNewChild(node, NULL, BAD_CAST "id", BAD_CAST buffer);
    xmlNewChild(node, NULL, BAD_CAST "active", BAD_CAST "true");
    // get the student name
    printf("Enter student name: ");
    if (fgets(buffer, MAXCHARS, stdin) == NULL) {
      fprintf(stderr, "failed to read string\n");
      return (EXIT_FAILURE);
    }
    strtok(buffer, "\n");
    xmlNewChild(node, NULL, BAD_CAST "name", BAD_CAST buffer); 
  }
  return (EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  xmlDocPtr doc = NULL;
  packedobjectsContext *pc = NULL;
  char *pdu = NULL;
  ssize_t bytes_sent;
  int       sock;                                                 
  struct    sockaddr_in servaddr;

  // setup socket
  if ( (sock = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
    fprintf(stderr, "Error creating socket.\n");
    exit(EXIT_FAILURE);
  }
  // setup addressing
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family      = AF_INET;
  servaddr.sin_addr.s_addr = inet_addr(HOST_IP);
  servaddr.sin_port        = htons(HOST_PORT); 
  
  // initialise packedobjects
  if ((pc = init_packedobjects(XML_SCHEMA, 0, 0)) == NULL) {
    printf("failed to initialise libpackedobjects");
    exit(1);
  }
  
  // create the data
  doc = xmlNewDoc(BAD_CAST "1.0");
  add_student_data(doc);
  
  // encode the XML DOM
  pdu = packedobjects_encode(pc, doc);
  if (pc->bytes == -1) {
    printf("Failed to encode with error %d.\n", pc->encode_error);
  }

  // make network connection
  if (connect(sock, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
    fprintf(stderr, "Error calling connect()\n");
    exit(EXIT_FAILURE);
  }
  
  // send the pdu across the network
  bytes_sent = send(sock, pdu, pc->bytes, 0);

  if (bytes_sent != pc->bytes) {
    fprintf(stderr, "Error calling send()\n");
    exit(EXIT_FAILURE);
  }

  if ( close(sock) < 0 ) {
    fprintf(stderr, "Error calling close()\n");
    exit(EXIT_FAILURE);
  }  
  
  // free the DOM
  xmlFreeDoc(doc);  
  
  // free memory created by packedobjects
  free_packedobjects(pc);
  
  xmlCleanupParser();
  return(0);
}
